Dr Joseph Rizk is a Specialist Plastic Surgeon who is an expert in surgical and non-surgical treatments. He is the supervising doctor of White Hill Medispa.

Address: Shop 1/2-6 Bridge Rd, Stanmore, NSW 2048, Australia

Phone: 1300 707 007

Website: http://www.drrizk.com.au
